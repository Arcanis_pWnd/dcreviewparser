import { Sequelize, DataTypes } from 'sequelize';
import config from 'config';

const sequelize = new Sequelize({
    dialect: 'sqlite',
    storage: config.get('sqlite.path'),
});

const ChainModel = sequelize.define("Chain", {
    name: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    externalID: {
        primaryKey: true,
        type: DataTypes.INTEGER,
        allowNull: false,
        unique: true
    },
    url: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true
    },
    createdAt: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW
    }
});

const ReviewModel = sequelize.define("Review", {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    author: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    body: {
        type: DataTypes.TEXT,
        allowNull: false,
    },
    rate: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    ratedAt: {
        type: DataTypes.DATE,
        allowNull: false,
    },
    createdAt: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW
    },
    answers: {
        type: DataTypes.TEXT,
        allowNull: true
    }
});

ChainModel.hasMany(ReviewModel, {
    foreignKey: "chainID",
});

ReviewModel.belongsTo(ChainModel, { foreignKey: "chainID" });

const syncDB = async () => {

    await sequelize.sync({ alter: true });

}


module.exports = { ReviewModel, ChainModel, syncDB };
 