const parser = require('node-html-parser');
const util = require('util')
const request = require("request");


/*
 * Метод для парсинга ссылки на DeliveryClub и получение идентификатора цепочки отзывов
 * из исходного кода страницы
 */
exports.parseURL = async (url: string) => {

    // 1. Получаем название ресторана из URL, чтобы убедиться, что он валидный

    const splited_url = url.split('https://www.delivery-club.ru/srv/');

    if (splited_url.length !== 2) {
        return { error: true, msg: "Неверный URL" };
    }

    const first_part = splited_url[1];
    const name = first_part.split("/feedbacks")[0];

    if (name.length === 0) {
        return { error: true, msg: "Неверный URL" };
    }

    // 2. Получаем исходный код страницы

    let chainID = -1;

    const requestPromise = util.promisify(request);
    const response = await requestPromise(url)
    
    const root = parser.parse(response.body);

    const scripts = root.querySelectorAll('script')

    // 3. Ищем <script>, который хранит в себе информацию с идентификатором цепочки отзывов

    scripts.forEach(script => {

        const splited_script = script.toString().split('window.__INITIAL_STATE__=');

        if (splited_script.length === 2) {

            const json_str = splited_script[1].split('</script>')[0];

            chainID = JSON.parse(json_str).reviews.reviewsRequestParams.chainId;
        
        }
    
    })

    if (chainID === -1) {
        return { error: true, msg: "Не удалось получить индентификатор" };
    }

    return { name: name, chainID: Number(chainID), url: url };
}

/*
 * Метод для загрузки порции отзывов по идентификатору цепочки отзывов
 */
exports.loadReviewPortion = async (chainID: number, limit: number, offset: number) => {

    const api_url = 'https://api.delivery-club.ru/api1.2/reviews?chainId='+chainID+'&limit='+limit+'&offset='+offset;

    console.log(api_url);

    const requestPromise = util.promisify(request);
    const response = await requestPromise(api_url)
        
    const json_body = JSON.parse(response.body);

    return json_body;

}