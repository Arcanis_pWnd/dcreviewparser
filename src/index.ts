import express from 'express';
import config from 'config';

const routes = require("./routes");
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./../swagger.json');
const { syncDB } = require('./db');

syncDB().then(() => {

    const app = express();

    app.use(express.json());

    app.use("/api/v1/review", routes);

    const port = config.get('server.port');

    app.use('/', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

    app.listen(port, () => {
        console.log(`Server started at http://localhost:${port}`)
    });

});

