import express from 'express';

const router = express.Router();
const { getReview, loadReview } = require("./controller");

router.route("/getReview").get(getReview);
router.route("/startLoadReview").get(loadReview);

module.exports = router;