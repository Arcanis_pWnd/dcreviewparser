# DCReviewParser

Развернуть и запустить:

```
npm install
npm run build
npm run start
```

Swagger открывается по localhost:5000

Для загрузки отзывов был использован API-метод, найденный на странице отзывов 
```
https://api.delivery-club.ru/api1.2/reviews?chainId=###&limit=###&offset=###
```

Сам chainID был найден на станице с отзывами

На основе найденных инструментов был создан этот парсер ^^
