const { ReviewModel, ChainModel } = require('./db');
const { parseURL, loadReviewPortion } = require('./tools');
import config from 'config';

exports.getReview = async (req, res) => {
    const url = req.query.url; 
    const limit = req.query.limit;
    const offset = req.query.offset
    let sort = [];

    if (req.query.sortColumn && req.query.sortKeyword) {
        sort = [[req.query.sortColumn, req.query.sortKeyword]]
    }

    let records = [];
    let total = 0;

    try {

        // 1. Ищем идентификатор цепочки отзывов по URL

        const chainsM = await ChainModel.findAll({
            where : {
                url
            }
        })

        if (chainsM.length == 0) {

            return res.status(500).json({
                success : false,
                msg : "Цепочка отзывов по данному URL не найдена"
            })

        }

        const chainM = chainsM[0];

        const chainID = chainM.get('externalID');

        // 2. Получаем все отзывы по индентификатору цепочки отзывов вместе с фильтрацией

        records = await ReviewModel.findAll({
            where : {
                chainID
            },
            limit,
            offset,
            order : sort,
            raw : true
        })

        total = await ReviewModel.count();

    } catch(e) {

        console.log('Get reviews error', e);
        return res.status(500).json({
            success : false,
            msg : "Ошибка получения записей"
        })
        
    }

    return res.status(200).json({
        success : true,
        records,
        limit,
        offset,
        total
    })

}

exports.loadReview = async (req, res) => {

    const portions_limit = config.get('options.portions')
    let offset = 0;

    const asyncForEach = async (array, callback) => {
        for (let index = 0; index < array.length; index++) {
            await callback(array[index], index, array)
        }
    }

    parseURL(req.query.url).then((result) => {
    
        console.log(result)
    
        if (typeof(result.error) === 'undefined') {
    
            ChainModel.findOrCreate({ 
                where : {
                    name : result.name,
                    externalID : result.chainID,
                    url : result.url
                }
            }).then(async (chainM) => {

                let total_count = 0;

                do {
                    
                    await loadReviewPortion(result.chainID, portions_limit, offset).then(async (chain_result) => {
        
                        total_count = Number(chain_result.total)

                        await asyncForEach(chain_result.reviews, async (review) => {
        
                            try {
        
                                const reviewsM = await ReviewModel.findOrCreate({
                                    where : {
                                        author : review.author,
                                        body : review.body,
                                        rate : review.icon,
                                        ratedAt : new Date(review.rated).getTime(),
                                        chainID : result.chainID
                                    }
                                })
        
                                if (review.answers.length > 0) {
                                    
                                    const reviewM = reviewsM[0]
                                    
                                    reviewM.set({
                                        answers : JSON.stringify(review.answers)
                                    })
                                    
                                    await reviewM.save();
                                
                                }
        
                            } catch(e) {
                                    
                                console.log('ReviewModel create error: ',e)
                                
                            }
        
                        })
                        
                    });

                    offset += portions_limit;

                } while (total_count > portions_limit + offset);
            
            })
    
        }
    
    
    })

    return res.status(200).json({
        success : true
    })

}